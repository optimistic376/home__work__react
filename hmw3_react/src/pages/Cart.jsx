import React, {useState} from 'react';
import Card from '../components/card/Card';

export default function Cart({cart}){
    
    return (
        <div className="cart__page">
           <Card cart={cart} />
        </div>
    )
}
import React, {useState} from 'react'
import Card from '../components/card/Card'

export default function Favorites({favorites}){
    
        return (
            <div className="favorites__page">
              <Card favorites={favorites} />
            </div>
        )
}


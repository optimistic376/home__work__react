import { Fragment, useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import pages from './pages.scss';
import Modal from '../components/modal/Modal';
import CardList from '../components/cardList/CardList';
import Favorites from './Favorites';
import Cart from './Cart';


export default function Home() {
  const [active, setActive] = useState(false);
  const [currentId, setCurrentId] = useState(null);
  const [items, setItems] = useState([]);
  const [cartList, setCart] = useState([]);
  const [favoriteList, setFavorites] = useState([]);
  const [isInFavorites, setIsInFavorites] = useState(false);



  useEffect(() => {
    getItems()
  }, []);

  const getItems = () => {
    if (!localStorage.getItem('cart') || !localStorage.getItem('favorites')) {
      localStorage.setItem('cart', JSON.stringify([]));
      localStorage.setItem('favorites', JSON.stringify([]));
    }
      fetch('products.json')
        .then(response => response.json())
        .then(items => setItems(items));
    }


    const handleModal = (id) => {
      if (id) {
        setActive(!active)
        setCurrentId(id)

      } else {
        setActive(!active)
        setCurrentId(null)
      }
    }


    const addToCart = (id) => {
      const cart = JSON.parse(localStorage.getItem("cart"));
      const product = items.find((product) => product.dataId === id)
      cart.push(product)
      localStorage.setItem('cart', JSON.stringify(cart))
      setActive(!active)
      setCurrentId(null)
      setCart([...cartList, product])
    }
    console.log("Cart:", cartList)

    const addToFavorites = (id) => {
      const favorites = JSON.parse(localStorage.getItem("favorites"));
      const item = items.find((item) => item.dataId === id)
      favorites.push(item)
      localStorage.setItem('favorites', JSON.stringify(favorites));
      setFavorites([...favoriteList, item])
      // id.push(item.dataId)
      // setId([...idList, id])
    }
    console.log("Favorites:", favoriteList)

    return (
      // <Router>
      <Switch>
        <Route exact path="/" component={Home}>
          <CardList items={items} addToFavorites={addToFavorites} handleModal={handleModal} favorites={favoriteList} />
          <Fragment>
            <Modal
              isOpen={active}
              className="buy-modal active"
              header="Are you sure you want to add a product to your cart?"
              onSubmit={addToCart}
              onCancel={handleModal}
              id={currentId}
            />
          </Fragment>
        </Route>
        <Route path="/favorites" component={Favorites}>
       <CardList items={favoriteList} />
        </Route>
        <Route path="/cart" component={Cart}>
        <CardList items={cartList} />
        </Route>
      </Switch>
      // </Router>
   
    )
  }

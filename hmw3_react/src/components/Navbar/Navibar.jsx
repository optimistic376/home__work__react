import React, { Component } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import navibar from './navibar.scss';
import logo from './logo.png';
import Home from '../../pages/Home';

export default function Navibar (){

    return (
        <Router>
        <div className="header__menu">
            <Navbar collapseOnSelect expand="lg">
                <Navbar.Brand><img src={logo}></img></Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav"></Navbar.Toggle>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                       <Nav.Link><Link to="/">Home</Link></Nav.Link>
                       <Nav.Link><Link to="/favorites">Favorites</Link></Nav.Link>
                       <Nav.Link><Link to="/cart">Cart</Link></Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
           
        </div>
        <Home />
        </Router>
    )
}


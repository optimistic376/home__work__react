import React from 'react';
import card from "./card.scss";
import PropTypes, { number } from 'prop-types';
import Button from '../button/Button';
import Star from '../star/Star';


export default function Card ({vendorCode, src, name, price, dataId, addToFavorites, handleModal}){
  return (
    <>
      <div className="card" key={vendorCode}>
        <div className="card-body">
          <Star id = {dataId} onClick={() => {addToFavorites(dataId)}}/>
          <img className="card-body__img" src={src} alt="" />
          <h1 className="card-title">
            {name}
          </h1>
          <div className="card-footer">
            <span className="card-price">
              Price: {price} UAH
            </span>
          </div>
          <Button className="btn btn-primary" text="Add to cart" onClick={() => handleModal(dataId)} />
        </div>
      </div>
    </>
  )
}


Card.propTypes = {
  key: PropTypes.number,
  name: PropTypes.string,
  price: PropTypes.number, 
  addToFavorites: PropTypes.func,
  handleModal: PropTypes.func,
  src: PropTypes.string,
};

Card.defaultProps = {
  key: null,
  name: '',
  price: null,
  addToFavorites: null,
  handleModal: null,
  src: '',
};
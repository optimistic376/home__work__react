import {Component} from 'react';
import icon from './icon.scss';
import iconCancel from './icon-cancel.png';
import PropTypes from 'prop-types';

export default function Icon ({className, src, onClick}){
  
  return(
    <img className = {className} src = {src} onClick={onClick}></img>
)
}





Icon.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  src: PropTypes.string,
};

Icon.defaultProps = {
  className: '',
  onClick: null,
  src: '',
};


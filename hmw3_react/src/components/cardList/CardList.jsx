import React from 'react';
import Card from '../card/Card';
import PropTypes, { number } from 'prop-types';


export default function CardList ({items, addToFavorites, handleModal}) {

    return (
        <div className="card__list">
            {items.map((item) => (
                <div key={item.dataId}>
                    <Card {...item} handleModal={handleModal} addToFavorites={addToFavorites}/> 
                </div>
            ))}
        </div>
    )
}


CardList.propTypes = {
    items: PropTypes.array,
    addToFavorites: PropTypes.func,
    handleModal: PropTypes.func,
  };
  
CardList.defaultProps = {
    items: [],
    addToFavorites: null,
    handleModal: null,
  };
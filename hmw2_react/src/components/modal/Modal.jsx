import { Component, Fragment } from 'react';
import modal from './modal.scss';
import Button from '../button/Button';
import Icon from '../icon/Icon';
import PropTypes, { number } from 'prop-types';


export default class Modal extends Component {
    constructor(props) {
        super(props);
        console.log(this.props)
    }
  
    render() {
        const { isOpen, onSubmit, onCancel, className, id} = this.props;
        return (
            <>
                {isOpen &&
                    <div className={className} onClick={onCancel} id={id}>
                        <div className="modal__content" onClick={e => e.stopPropagation()}>
                            <span className="modal__close" onClick={onCancel}>&times;</span>
                            <header className="modal__header">{this.props.header}
                            </header>
                            <footer className="modal__footer">
                                <Button className="modal__btn submit" text="Ok" onClick={() => onSubmit(id)} />
                                <Button className="modal__btn cancel" text="Cancel" onClick={() => onCancel(id)} />
                            </footer>
                        </div>
                    </div>
                }
            </>
        );
    }

}

Modal.propTypes = {
    isOpen: PropTypes.bool,
    onSubmit: PropTypes.func,
    onCancel: PropTypes.func,
    src: PropTypes.string,
    className: PropTypes.string,
    id: PropTypes.number,
  };
  
Modal.defaultProps = {
    isOpen: false,
    onSubmit: () => {},
    onCancel: () => {},
    src: '',
    className: '',
    id: null,
  };

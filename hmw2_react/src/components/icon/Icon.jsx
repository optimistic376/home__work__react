import {Component} from 'react';
import icon from './icon.scss';
import iconCancel from './icon-cancel.png';
import PropTypes from 'prop-types';

class Icon extends Component {
    constructor(props){
        super(props);
    }

    render(){
        const {className, src, onClick} = this.props;
        return(
            <img className = {className} src = {src} onClick={onClick}></img>
        )
    }
}

export default Icon;



Icon.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  src: PropTypes.string,
};

Icon.defaultProps = {
  className: '',
  onClick: null,
  src: '',
};


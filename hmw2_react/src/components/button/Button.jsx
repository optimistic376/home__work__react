import {Component} from 'react';
import button from './button.scss';
import PropTypes, { number } from 'prop-types';




class Button extends Component{
  constructor(props){
    super(props);
  }
render(){
  const {className, onClick, text} = this.props;
  return (
    <button className={className} onClick={onClick}>
      {text}
    </button>
  );
}

}

export default Button;

Button.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  text: PropTypes.string,
};

Button.defaultProps = {
  className: '',
  onClick: null,
  text: '',
};
import React, { Component } from 'react';
import Card from '../card/Card';
import PropTypes, { number } from 'prop-types';


class CardList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {items, addToFavorites, handleModal, favorites} = this.props;
        return (
            <div className="card__list">
                {items.map((item) => (
                    <div key={item.dataId}>
                        <Card {...item} handleModal={handleModal} addToFavorites={addToFavorites} favorites={favorites}/> 
                    </div>
                ))}

            </div>
        )
    }
}

export default CardList

CardList.propTypes = {
    items: PropTypes.array,
    addToFavorites: PropTypes.func,
    handleModal: PropTypes.func,
  };
  
CardList.defaultProps = {
    items: [],
    addToFavorites: null,
    handleModal: null,
  };
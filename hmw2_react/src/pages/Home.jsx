import { Component, Fragment } from 'react';
import pages from './pages.scss';
import Modal from '../components/modal/Modal';
import CardList from '../components/cardList/CardList';



class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      active: false,
      btn: false,
      currentId: null,
      favorites: JSON.parse(localStorage.getItem('favorites')) || [],
    };

    this.handleModal = this.handleModal.bind(this)
    this.addToCart = this.addToCart.bind(this)
  }

  handleModal(id) {
    if (id) {
      this.setState((state) => ({ ...state, active: !state.active, currentId: id })) // если айди пришло записывает его в state родителя и меняет значение active(хранится в родительском state) на противоположное //
    } else {
      this.setState((state) => ({ ...state, active: !state.active, currentId: null })) // если нет айди меняет значение active(хранится в родительском state) на противоположное //
    }
  }

  addToCart = (id) => {
    console.log(`Add to cart: ${id}`)
    let cart = JSON.parse(localStorage.getItem("cart"));
    let product = this.state.items.find((product) => product.dataId === id)
    cart.push(product)
    localStorage.setItem('cart', JSON.stringify(cart))
    this.setState((state) => ({ ...state, active: !state.active, currentId: null }))
  }
  // addToFavorites = (id) => {
  //   const items = this.state.items;
  //   let favorites = JSON.parse(localStorage.getItem("favorites"));
  //   let item = items.find((item) => item.dataId === id)
  //   favorites.push(item)
  //   localStorage.setItem('favorites', JSON.stringify(favorites));
  // }
    addToFavorites = (id) => {
    const items = this.state.items;
    let favorites = this.state.favorites;
    let item = items.find((item) => item.dataId === id)
    favorites.push(item.dataId)
    this.setState(favorites)
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }


  componentDidMount() {
    if(!localStorage.getItem('cart') || !localStorage.getItem('favorites')) {
      localStorage.setItem('cart', JSON.stringify([]));
      localStorage.setItem('favorites', JSON.stringify([]));
    }
    fetch("products.json")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result.items
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }


  render() {
    const { error, isLoaded, items, currentId, favorites } = this.state;
    if (error) {
      return <div>Помилка: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Завантаження...</div>;
    } else {
      return (
        <>
          <CardList items={items} handleModal={this.handleModal.bind(this)} addToFavorites={this.addToFavorites.bind(this)} favorites={favorites}/>
          <Fragment>
            <Modal
              isOpen={this.state.active}
              className="buy-modal active"
              header="Are you sure you want to add a product to your cart?"
              onSubmit={this.addToCart}
              onCancel={this.handleModal.bind(this)}
              id = {currentId}
            />
          </Fragment>
        </>
      );
    }
  }

}

export default Home;
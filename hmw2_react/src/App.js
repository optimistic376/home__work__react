
import './App.scss';
import { Component} from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Header from './components/header/Header';
import Home from './pages/Home';
import Favorites from './pages/Favorites';
import Cart from './pages/Cart';


class App extends Component{
    constructor(props){
      super(props);
  
    }
  
  render(){
    return (
  <>
  <Router>
  <Header />
  <Switch>
      <Route exact path="/" component={Home}></Route>
      <Route path="/favorites" component={Favorites}></Route>
      <Route path="/cart" component={Cart}></Route>
  </Switch>
  </Router>
  </>
    );
  }
  
  }
  
  export default App;








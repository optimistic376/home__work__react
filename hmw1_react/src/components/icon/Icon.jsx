import {Component} from 'react';
import icon from './icon.scss';
import iconCancel from './icon-cancel.png'

class Icon extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <img className = {this.props.className} src = {iconCancel} onClick={this.props.onClick}></img>
        )
    }
}

export default Icon;
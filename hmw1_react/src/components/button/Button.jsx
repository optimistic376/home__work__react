import {Component} from 'react';
import button from './button.scss';




class Button extends Component{
  constructor(props){
    super(props);
  }
render(){
  return (
    <button className={this.props.className} style={{background: this.props.backgroundColor}} onClick={this.props.onClick}>
      {this.props.text}
    </button>
  );
}

}

export default Button;
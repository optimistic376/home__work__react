import {Component, Fragment} from 'react';
import modal from './modal.scss';
import Button from '../button/Button';
import Icon from '../icon/Icon';


class Modal extends Component{
    constructor(props){
        super(props);

    
}
closeModalButton() {

    return (

        <Fragment>
              <Icon className = "modal__header-cancel" onClick={this.props.onClick}/>
        </Fragment>
    )
}

     render(){
        return (
            <>
            <div className = {this.props.className} onClick={this.props.onClick} closebutton = {this.props.closeButton}>
                <div className="modal__content" onClick={e => e.stopPropagation()}>
                <header className="modal__header">{this.props.header}
                {this.props.closeButton ? this.closeModalButton() : ""}
                </header>
                <main className="modal__main">{this.props.text}</main>
                <footer className="modal__footer">
                {/* <Button className="modal__btn submit" text="Ok"/>
                <Button className="modal__btn cancel" text="Cancel"/> */}
                {this.props.actions}
                </footer>
                </div>
            </div>
            </>
        );
     } 
    
}

export default Modal;
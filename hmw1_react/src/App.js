
import './App.scss';
import { Component, Fragment } from 'react';
import Button from './components/button/Button';
import Modal from './components/modal/Modal';



class App extends Component{
    constructor(props){
      super(props);
  
      this.state = {
        active: false,
        btn1: false,
        btn2: false,
      }
  
      this.handleClick = this.handleClick.bind(this)
      this.removeActive = this.removeActive.bind(this)
    }

  handleClick(event){
      if (event.target.className === "btn__modal-first") {
              this.setState({
                btn1: true,
                btn2: false,
              })
            } else if (event.target.className === "btn__modal-second") {
              this.setState({
                btn2: true,
                btn1: false,
              })
            }
        
            this.setState({ active: true })
  }
  
  removeActive(event){
    if (event.target.className === "modal active" || event.target.className === "modal__header-cancel") {
           this.setState({active: false})
          }
  }

  modalActions(text_btn_ok, text_btn_cancel) {

    return (

      <div className="wrapper">
        <Button className="modal__btn ok" text={text_btn_ok} bcgcolor="navy" width="200px" />
        <Button className="modal__btn cancel" text={text_btn_cancel} bcgcolor="navy" width="200px" />
      </div>
    )
    }
  
  render(){
    return (
      <Fragment>
        <div className="wrapper">
          <Button className="btn__modal-first" text="Open first modal" backgroundColor="green" onClick={this.handleClick}/>
          <Button className="btn__modal-second" text="Open second modal" backgroundColor="blue" onClick={this.handleClick}/>
      </div>
      {this.state.active && this.state.btn1 ?
      <Modal 
         className = "modal active"
          header="First modal" 
          text="Every day i spend my time, drinking wine, feeling fine. Do you want to try it with me?" 
          onClick = {this.removeActive}
          closeButton={false}
          actions={this.modalActions("Yes", "No")}>
      </Modal> : ""
        }
      {this.state.active && this.state.btn2 ?
      <Modal 
          className = "modal active"
          header="Do you want to delete this file?" 
          text="Once you delete this file, it want be possible to undo this action. Are you sure you want to delete it?" 
          onClick = {this.removeActive}
          closeButton={true}
          actions={this.modalActions("Ok", "Cancel")}>
      </Modal> : ""
      }  
   </Fragment>
    );
  }
  
  }
  
  export default App;







